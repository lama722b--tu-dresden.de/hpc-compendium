# Electromagnetic Field Simulation

The following applications are installed at ZIH:

|          |          |            |
|----------|----------|------------|
|          | **Mars** | **Deimos** |
| **HFSS** |          | 11.0.2     |

## HFSS

[HFSS](http://www.ansoft.com/products/hf/hfss/) is the industry-standard
simulation tool for 3D full-wave electromagnetic field simulation. HFSS
provides E- and H-fields, currents, S-parameters and near and far
radiated field results.

-- Main.mark - 2010-01-06

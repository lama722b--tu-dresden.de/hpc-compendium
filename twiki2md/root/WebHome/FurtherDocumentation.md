# Further Documentation



## Libraries and Compiler

-   <http://www.intel.com/software/products/mkl/index.htm>
-   <http://www.intel.com/software/products/ipp/index.htm>
-   <http://www.ball-project.org/>
-   <http://www.intel.com/software/products/compilers/> - Intel Compiler
    Suite
-   <http://www.pgroup.com/doc> - PGI Compiler
-   <http://pathscale.com/ekopath.html> - PathScale Compilers

## Tools

-   <http://www.allinea.com/downloads/userguide.pdf> - Allinea DDT
    Manual
-   <http://www.totalviewtech.com/support/documentation.html> -
    Totalview Documentation
-   <http://www.gnu.org/software/gdb/documentation/> - GNU Debugger
-   <http://vampir-ng.de> - official homepage of Vampir, an outstanding
    tool for performance analysis developed at ZIH.
-   <http://www.fz-juelich.de/zam/kojak/> - homepage of KOJAK at the FZ
    Jlich. Parts of this project are used by Vampirtrace.
-   <http://www.intel.com/software/products/threading/index.htm>

## OpenMP

You will find a lot of information at the following web pages:

-   <http://www.openmp.org>
-   <http://www.compunity.org>

## MPI

The following sites may be interesting:

-   <http://www.mcs.anl.gov/mpi/> - the MPI homepage.
-   <http://www.mpi-forum.org/> - Message Passing Interface (MPI) Forum
    Home Page
-   <http://www.open-mpi.org/> - the dawn of a new standard for a more
    fail-tolerant MPI.
-   The manual for SGI-MPI (installed on Mars ) can be found at:

<http://techpubs.sgi.com/library/manuals/3000/007-3773-003/pdf/007-3773-003.pdf>

## SGI developer forum

The web sites behind
<http://www.sgi.com/developers/resources/tech_pubs.html> are full of
most detailed information on SGI systems. Have a look onto the section
'Linux Publications'. You will be redirected to the public part of SGI's
technical publication repository.

-   Linux Application Tuning Guide
-   Linux Programmer's Guide, The
-   Linux Device Driver Programmer's Guide
-   Linux Kernel Internals.... and more.

## Intel Itanium

There is a lot of additional material regarding the Itanium CPU:

-   <http://www.intel.com/design/itanium/manuals/iiasdmanual.htm>
-   <http://www.intel.com/design/archives/processors/itanium/index.htm>
-   <http://www.intel.com/design/itanium2/documentation.htm>

You will find the following manuals:

-   Intel Itanium Processor Floating-Point Software Assistance handler
    (FPSWA)
-   Intel Itanium Architecture Software Developer's Manuals Volume 1:
    Application Architecture
-   Intel Itanium Architecture Software Developer's Manuals Volume 2:
    System Architecture
-   Intel Itanium Architecture Software Developer's Manuals Volume 3:
    Instruction Set
-   Intel Itanium 2 Processor Reference Manual for Software Development
    and Optimization
-   Itanium Architecture Assembly Language Reference Guide
